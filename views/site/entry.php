<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin() ?>

<?= $form->field($model, 'name')->textInput()->input('name', ['placeholder' => 'Enter Your Name'])->label("Your Name") ?> 
<?= $form->field($model, 'email')->label('Your Email')->textInput()->input('email', ['placeholder' => 'Enter Your Email']) ?>

<div class="form-group">
    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>